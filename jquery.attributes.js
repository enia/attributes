/**
 * Attributes v1.0
 *
 * A jQuery extension which listens and collects data from user-selected attributes.
 * When an attribute is selected, next can be activated.
 */
(function($) {
	'use strict';
	
	if($.attributes) {
		return;
	}
	
	$.attributes = function(container, options) {
		var self = this;
		
		this.ct = container;
		this.options = options;
		
		this.rows = this.ct.find('.attributeslist > .attribute').each(function(){
			if($(this).data('attribute')) {
				self.attr[$(this).data('attribute')] = new $.attributes.attribute(this, self);
			}
		});
		
		this.reset();
		
		this.options.init.call( this, this );
	};
	
	$.attributes.prototype = {
		index : 0,
		attr : {},
		reset : function() {
			for(var idx in this.attr) {
				if(this.attr[idx]){
					this.attr[idx].enable();
				}
			}
			//this.attr[this.index].enable();
			//this.cur = this.attr[0];
			/*
			this.rows.addClass('disabled');
			
			var cur = $(this.rows.get(0));
			cur.removeClass('disabled').find('input').prop('disabled', false);
			cur.find('li.disabled').removeClass('disabled');
			
			/*
			if(!cur.hasClass('required') && $(this.rows.get(this._getCurrentRow()+1)).length) {
				this.next();
			}*/
			return this;			
		},
		next : function() {
			this.index++;
			this.cur = typeof(this.attr[this.index]) !== undefined ? this.attr[this.index] : null;
			if(this.cur) {
				this.cur.enable();	
			}
		},
		current : function(){
			return this.attr[this.index];
		},
		isLast : function() {
			
		},
		updateAll : function(list) {
			var self = this;
			$.each(list, function(i, v){
				if(self.attr[i]) {
					$.each(v, function(ii, iv){
						self.attr[i].update(ii, iv);
					});
				}
			});
			/*
			var self = this, regexr = [];
			for(var idx in this.attr) {
				if(this.attr[idx]){
					regexr.push(this.attr[idx].value() ? this.attr[idx].value() : '[0-9]+');
				}
				//this.attr[idx].disableAll();
			}

			var regex = new RegExp(regexr.join('\-'));
			$.each(list, function(i, v){
				if(regex.test(i)) {
					for(var x in v) {
						if(self.attr[x]) {
							self.attr[x].update(v[x], v);
						}
					}
				}
			});
			/*
			for(var idx in this.attr) {
				if(this.attr[idx]){
					
					
					console.log(this.attr[idx].getOptions());
				}
			}*/
		},
		values : function() {
			var o = {};
			$.each(this.attr, function(){
				o = $.extend(o, this.toValueObject());
			});
			return o;
		},
		getValues : function() { return this.values(); }
	};
	
	$.attributes.attribute = function(el, parent) {
		this.parent = parent;
		this.el = $(el);
		this._value = null;
		
		var self = this, timer;
	
		var onChangeHandler = function(e) {
			e.preventDefault();
			
			var el = $(e.target).closest('li').find('input'), val = null;
			
			self.toggle(el.val());
			
			el.each(function() {
				if(($(this).is(':radio') || $(this).is(':checkbox')) && $(this).is(':checked')) {
					val = val + $(this).val();
				} else if($(this).is(':text') || $(this).attr('type') === 'textarea' || $(this).attr('type') === 'number') {
					val = val + $(this).val();
				}		
			});

			if(self._value != val) {
				self._value = val;
				$(self).trigger('change', [el]);
			}
		};
		
		$(this).on('change', function() {
			clearTimeout(timer);
			timer = setTimeout(function(){
				self.parent.options.change.call( self, self.parent );
			},200);
		});
		
		this.el.on('keyup', 'input[type="text"], input[type="number"]', onChangeHandler)
			   .on('change', 'input[type="number"]', onChangeHandler)
			   .on('click', 'li:not(:has(input[type="number"]))', onChangeHandler);
	};
	$.attributes.attribute.prototype = {
		enable : function(val) {
			if(val){
				this.el.find('input[value="'+val+'"]').closest('li').removeClass('disabled');
			} else {
				this.el.removeClass('disabled');
			}
			return this;
		},
		disable : function(val) {
			if(val){
				this.el.find('input[value="'+val+'"]').closest('li').addClass('disabled');
				if(this.isDisabled(val)) {
					//this.deselect();
				}
			} else {
				this.el.addClass('disabled');
				this.deselect();
			}
			return this;
		},
		type : function() {
			switch(this.el.find('input').attr('type')) {
				case 'radio':
					return 'select';
					break;
				case 'text':
				default:
					return 'text';
			}
		},
		toggle : function(val) {
			this[this.isSelected(val) ? 'deselect' : 'select'](val);
		},
		isSelected : function(val) {
			return this.el.find('input[value="'+val+'"]').closest('li').hasClass('selected');
		},
		select : function(val) {
			this.deselect();
			if(!this.isDisabled()) {
				var el = this.el.find('input[value="'+val+'"]').attr('checked', true).closest('li');
				el.addClass('selected');
			}
			return this;
		},
		deselect : function() {
			this.el.find('li').removeClass('selected').find('input:checked').removeAttr('checked');
			return this;
		},
		isDisabled : function(val) {
			if(val){
				return this.el.find('input[value="'+val+'"]').closest('li').hasClass('disabled');
			} 
			return this.el.hasClass('disabled');
		},
		update : function(val, enabled) {
			this[enabled?'enable':'disable'](val);
		},
		getOptions : function() {
			var a = [];
			this.el.find('input[type=radio]').each(function(i, o){
				a.push($(o).attr('value'));
			});
			return a;
		},
		value : function() {
			return this.toValueObject()[this.name()];
		},
		name : function() {
			for (var o in this.toValueObject()) { if(o) { return o; } }
		},
		toValueObject : function() {
			return this.el.find('input[type=text], input[type=number], input[type=radio]:checked').serializeObject();
		}
	};
	
	$.attributes.defaults = {
		init : function(){},
		change : function(){},
		complete : function(){},
		reset : function(){}
	};
	
	$.fn.attributes = function(opts){
		opts = $.extend( true, {}, $.attributes.defaults, opts );
		return this.each(function() {
			var obj = new $.attributes( $(this), opts );
			$(this).data( 'attributes', obj );
		});
	};
})(jQuery);